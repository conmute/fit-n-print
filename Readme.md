# Fit-N-Print

## Instructions

Flow for project

```bash
yarn install
yarn watch
yarn build
```

CLI for phantomjs

```bash
npm install phantomjs -g
phantomjs rasterize.js http://localhost:1234/ out/index.pdf
```

**Warning!** Phantomjs package is deprecated, solution should go towards `phantomjs-prebuilt` package.

## ToDo

* [ ] **Implementation**: Support of complex variations of element placements,
      those that have overlapping top and bottom edges.

      In order to support 3d solution we need to split vertical zones into part and treat them separatelly for each element,

      Each auto-adjusted element should be broken into parts, in case if tob and bottom odjes overlaps with neighbours. These parts are treated separatelly and contain specific calculations withing, in such way that total sum of auto-adjusted height should be not more than allowed amount of height, in order to keep PDF single paged.

* [ ] **Bug**: During tests we faced that for autoadjusted elements that have no paddings-margings between happend to have broken layout.